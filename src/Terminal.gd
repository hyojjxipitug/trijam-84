extends Node2D


export var location: String = ""




func _on_InteractionSpace_body_entered(body: Node) -> void:
	WorldState.location = location




func _on_InteractionSpace_body_exited(body: Node) -> void:
	WorldState.location = ""
