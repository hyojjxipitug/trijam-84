extends Control
class_name BotUI

signal user_text_entered

export(String, MULTILINE) var message: String = "This is a test"

onready var output_text: RichTextLabel = $VBoxContainer/Output/RichTextLabel
onready var input_text: LineEdit = $VBoxContainer/Input

func _ready() -> void:
	output_text.bbcode_text = message
	input_text.grab_focus()

func _on_Input_text_entered(new_text: String) -> void:
	if new_text != "":
		output_text.append_bbcode("\n > " + new_text)
		input_text.clear()
		emit_signal("user_text_entered", new_text)


func append_text(new_text: String) -> void:
	output_text.append_bbcode("\n" + new_text)
	


