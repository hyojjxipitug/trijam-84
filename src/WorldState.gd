extends Node


signal light_state_changed
signal door_state_changed
signal pod_launched

var is_bot_active: bool = false
var is_pod_active: bool = false
var is_pod_escape_authorized: bool = false
var is_light_on: bool = false setget _set_is_light_on
var inventory: Array = []
var location: String = "" setget _set_location

var _doors: Dictionary = {
	"bedroom": false,
	"kitchen_door": false,
	"cockpit_door": false,
	"pod_bay_door": false
}


func _set_is_light_on(value: bool) -> bool:
	is_light_on = value
	emit_signal("light_state_changed", value)
	return value


func open_door(door_name: String) -> void:
	_doors[door_name] = not _doors[door_name]
	emit_signal("door_state_changed", door_name, _doors[door_name])
	

func _ready() -> void:
	print("WorldState autoloaded")
	

func _set_location(value: String) -> String:
	location = value
	print(value)
	return value
