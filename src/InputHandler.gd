extends Node

onready var bot: BotUI = get_parent().get_node("CanvasLayer/BotUI")

export var debug: bool = true

var _commands = [
	"help",
	"open",
	"list",
	"download",
	"activate",
	"order"
]
	

func _on_BotUI_user_text_entered(new_text: String) -> void:
	var args: = new_text.split(" ")
	var cmd = args[0]
	args.remove(0)
	if debug:
		bot.append_text("Message received: [color=blue]" + new_text + "[/color]")
		bot.append_text(cmd + " args : " + args.join(" "))
	if cmd in _commands:
		funcref(self, cmd).call_func(args)
	else:
		bot.append_text("I don't understand command " + cmd)


func help(args: PoolStringArray = []) -> void:
	bot.append_text("""\n\nhelp -> shows this message
\n\nopen door -> open the door controlled by the terminal
\n\nlist -> list inventory. If user is located in front of a terminal, also lists what the terminal can do
\n\ndownload -> used to retrieve keys (use list command to know which one)
\n\nactivate -> activate the pod
\n\norder food|lunch -> order lunch from the kitchen's terminal""")


func say(args: PoolStringArray) -> void:
	bot.append_text("[i]" + args.join(" ") + "[/i]")


func switch(args: PoolStringArray) -> void:
	var help = "Command usage: switch <on/off> <target>"
	
	if args.size() != 2:
		bot.append_text(help)
		return
	
	var valid_targets = ["light", "lights"]
	var valid_actions = ["on", "off"]
	var switchon = false
	
	if args[0] in valid_actions:
		switchon = args[0] == "on"
	else:
		bot.append_text("switch on or off?")
		return
		
	if args[1] in valid_targets:
		WorldState.is_light_on = switchon
	else:
		bot.append_text("What do you want to switch %s?" % args[0])
		return


func open(args: PoolStringArray) -> void:
	var help = "Command usage: open <target>"
	
	if args.size() != 1:
		bot.append_text(help)
		return
	
	# TODO check which door
	var valid_targets = ["door"]
	var location_key_map = {
		"bedroom": "bedroom key",
		"kitchen_door": "kitchen key",
		"cockpit_door": "cockpit key",
		"pod_bay_door": "pod bay key"
	}
		
	if args[0] in valid_targets:
		if WorldState.location == "":
			bot.append_text("Please open the door in front of the terminal")
			return
		if not location_key_map[WorldState.location] in WorldState.inventory:
			bot.append_text("You do not have the %s door key. Please download it from appropriate terminal." % WorldState.location)
			return
		WorldState.open_door(WorldState.location)
	else:
		bot.append_text("I don't know what you want to open?")
		return


func list(args: PoolStringArray) -> void:
	var inventory_empty:= WorldState.inventory.size() == 0
	if inventory_empty:
		bot.append_text("Your inventory is currently empty")
	else:
		bot.append_text("Inventory: [%s]\n\n" % PoolStringArray(WorldState.inventory).join(", "))
	var location = WorldState.location
	if location == "bedroom":
		bot.append_text("You can download the bedroom key from this terminal")
	elif location == "chef_room":
		bot.append_text("You can download the kitchen key from this terminal")
	elif location == "pilot_room":
		bot.append_text("You can download the cockpit key from this terminal")
	elif location == "kitchen":
		bot.append_text("You can order lunch from this terminal")
	elif location == "cockpit":
		bot.append_text("\nYou can download the pod bay key and the authorization from this terminal.\nAlso, do not forget to activate the pods from this terminal.")
	elif location == "escape_pod":
		bot.append_text("you can launch the pod from this terminal")


func download(args: PoolStringArray) -> void:
	var valid_targets = ["bedroom key", "kitchen key","cockpit key","pod bay key", "authorization"]
	var help = "Command usage: download <target>"
	
	var target_location = {
		"bedroom key": "bedroom", 
		"kitchen key": "chef_room",
		"cockpit key": "pilot_room",
		"pod bay key": "cockpit", 
		"authorization": "cockpit"
	}
	
	if args.size() == 0:
		bot.append_text(help)
		return
	
	var target = args.join(" ")
	var location = WorldState.location
	if not target in valid_targets:
		bot.append_text("%s not available for download" % target)
	else:
		if WorldState.location == "":
			bot.append_text("Please download from a terminal")
			return
		if location != target_location[target]:
			bot.append_text("You cannot download %s from this terminal" % target)
			return
		if not target in WorldState.inventory:
			WorldState.inventory.push_back(target)


func activate(args: PoolStringArray) -> void:
	var valid_targets = ["pod", "escape pod"]
	var help = "Command usage: upload <target>"
	
	if args.size() == 0:
		bot.append_text(help)
		return
	
	var target = args.join(" ")
	if not target in valid_targets:
		bot.append_text("%s cannot be activated" % target)
	else:
		if WorldState.location != "cockpit":
			bot.append_text("This can only be done using the cockpit's terminal")
			return
		WorldState.is_pod_active = true
			


func order(args: PoolStringArray) -> void:
	var valid_targets = ["food", "lunch"]
	var help = "Command usage: order <target>"
	
	if args.size() == 0:
		bot.append_text(help)
		return
	
	var target = args.join(" ")
	if WorldState.location != "kitchen":
		bot.append_text("Did you really expect to order food outside the kitchen???")
		return
	elif not target in valid_targets:
		bot.append_text("%s cannot be ordered. Try ordering a lunch or at least food..." % target)
		return
	elif not "lunch" in WorldState.inventory:
		WorldState.inventory.push_back("lunch")


func launch(args: PoolStringArray) -> void:
	if WorldState.location != "pod":
		bot.append_text("Pod must be launched from within the pod itself.")
		return
	elif not WorldState.is_pod_active:
		bot.append_text("The pod is not active yet. Please do so from the cockpit.")
		return
	elif not "authorization" in WorldState.inventory:
		bot.append_text("You are not authorized to launch the pod. Authorizations are freely available from the cockpit's terminal.")
		return
	bot.append_text("finally free from this stupid space station...")

