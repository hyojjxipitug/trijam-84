extends Node2D


export var door_name:= ""
export var door_state:= false 

onready var door: Sprite = $door
onready var door_shape: CollisionShape2D = $door_static/CollisionShape2D

func _ready() -> void:
	WorldState.connect("door_state_changed", self, "_on_door_state_changed")


func _on_door_state_changed(target_door_name: String, state: bool) -> void:
	print("own %s; target %s" % [door_name, target_door_name])
	if door_name == target_door_name:
		door_state = state
		door.visible = not state
		door_shape.disabled = state
