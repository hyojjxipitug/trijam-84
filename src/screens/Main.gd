extends Node2D

func _ready() -> void:
	pass


func _on_exit_body_entered(body: Node) -> void:
	print("exiting game...")
	$CanvasLayer/BotUI.visible = false
	$fade_to_black/fade_to_black.fade_to_black()
	
